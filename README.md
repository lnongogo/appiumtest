# AppiumTest 

Appium test


+**Prerequisites**


 * This project needs **Appium Studio** installed on the operating machine. 

  * User system variables Java_Home and ANDROID_HOME need to be set correctly, for referrence go to: https://docs.oracle.com/cd/E19182-01/821-0917/6nluh6gq9/index.html  (Java_Home). https://www.dev2qa.com/how-to-set-android-sdk-path-in-windows-and-mac/ (Android Home)


 # Installing
    "Once the **Prerequisites** are met, clone the project to the desktop."
    * Launch the Appium Studio IDE.
    * Ensure the device is correctly set up using steps found on the link below:
https://www.howtogeek.com/125769/how-to-install-and-use-abd-the-android-debug-bridge-utility/
    * On the Menu tab, Click on devices and **ADD** the device used for the test.
    * Once device has been added, click on **Import/Sign Application**.
    * You will then be encounter a message box asking to install the application on the device.
    * Click Yes and continue. 
    * DO NOT LAUNCH the application when promted to do so.
    
 **Next**
    * Go to **File** on the menu tab.
    * Open project folder.
    
    

# Running the test
 
*Once the project folder is loaded*
    * On the right-side of the IDE.
    * There will be a tab showing the TEST:AppiumTest.
    * Click on the Run Test button (Green)
    
# Author: **Lonwabo Nongogo
